sudo yum update
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
. ~/.nvm/nvm.sh
nvm install 8.11.1
sudo npm install -g @angular/cli@1.7.4
sudo git clone https://gitlab.com/Cloud-Computing-SoSe2018/front-end-angular.git
sudo chmod -R 777 front-end-angular
cd front-end-angular
sudo npm install
ng serve --host=0.0.0.0 --port=4200
