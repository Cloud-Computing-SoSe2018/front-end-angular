import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgModule } from '@angular/core';
import { MaterialComponentsModule } from './app.material.module';
import { AppRoutingModule } from './app.routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppComponent } from './app.component';
import { HomeComponent } from './modules/home/home.component';
import { LoginComponent } from './modules/login/login.component';
import { HeaderComponent } from './modules/header/header.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { CommonApiService } from './api/services/common-api.service';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FileUploadComponent } from './modules/file-upload/file-upload.component';


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        HeaderComponent,
        FileUploadComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MaterialComponentsModule,
        AppRoutingModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        HttpModule,
        HttpClientModule,
    ],
    providers: [
        HttpClient,
        CookieService,
        CommonApiService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
