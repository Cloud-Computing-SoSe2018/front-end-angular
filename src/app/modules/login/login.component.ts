import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { ErrorStateMatcher } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormArray, FormGroupDirective, NgForm } from '@angular/forms';
import { CommonApiService } from '../../api/services/common-api.service';
import { CookieService } from 'ngx-cookie-service';
import { HeaderComponent } from '../header/header.component';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [HeaderComponent]
})


export class LoginComponent implements OnInit {
  @Output() userLogin: EventEmitter<boolean> = new EventEmitter<boolean>();
  getUserObj: any;

  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', Validators.required)
  });
  matcher = new MyErrorStateMatcher();
  isLoading: boolean = false;
  profilePic: any;
  signUpData = {};
  disableBtn = false;
  errorMsg = "";
  url = "";
  profilePicUploaded = false;
  signUpForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    userName: new FormControl('', [Validators.required]),
    signupPassword: new FormControl('', Validators.required),
    emailId: new FormControl('', [Validators.required]),
    mobile: new FormControl('', Validators.required),
    gender: new FormControl('', Validators.required)
  });
  constructor(private api: CommonApiService,
    private cookie: CookieService,
    private router: Router,
    private HeaderComponent: HeaderComponent, ) { }

  ngOnInit() {
  }

  login() {
    let loginData = this.loginForm.value;
    this.isLoading = true;
    this.api.login(loginData)
      .subscribe(response => {
        this.isLoading = false;
        if (response.error) {
          this.api.showSnackbar('Invalid login credentials')
        }
        if (response['token']) {
          var data = {
            username: response.username,
            id: response.user_id,
            role: response.role,
            firstName: response.first_name,
            lastName: response.last_name,
            image: response.image
          }

          localStorage.setItem('userObj', JSON.stringify(data));
          this.cookie.set('token', response['token']);
          this.api.changeMessage("login");
          this.api.showSnackbar('Login Successfull');
          this.HeaderComponent.showUserImage('False');
          this.router.navigate(['home']);
        }
      })
  }

  signUp(userType) {
    this.disableBtn = true;
    this.api.showSnackbar('Please wait!');
    this.signUpData = {
      "first_name": this.signUpForm.controls['firstName'].value,
      "last_name": this.signUpForm.controls['lastName'].value,
      "email": this.signUpForm.controls['emailId'].value,
      "username": this.signUpForm.controls['userName'].value,
      "password": this.signUpForm.get('signupPassword').value,
      "mobile": this.signUpForm.get('mobile').value,
      "gender": this.signUpForm.get('gender').value,
      "role": "1",
      "image": this.profilePic,
      "location": "Fulda, DE",
      "bio": "Fassak!!"
    }
    this.url = "users/";


    this.isLoading = true;
    console.log('signup data =', this.signUpData);
    var data = new FormData();
    for (var key in this.signUpData) {
      data.append(key, this.signUpData[key]);
    }
    console.log("form Data == ", data);
    this.api.signup(data, this.url).subscribe(response => {
      this.disableBtn = false;
      if (response.error) {
        this.api.showSnackbar('Something went wrong');
        console.log('error message =', response.error['error'].email, response.error['error'].username);
        if (response.error.error.email && response.error.error.email[0] == 'Enter a valid email address') {
          this.errorMsg = 'Enter a valid email address';
        }
        if (response.error['error'].username) {
          this.errorMsg = 'Username alerady exists';
        }
        if (response.error.error.email && response.error.error.email[0] == 'This field must be unique.') {
          this.errorMsg = 'Email already exists';
        }
        this.isLoading = false;
        return;
      } else {
        this.isLoading = false;
        console.log('response after signup =', response);
        this.api.changeMessage("login");
        this.api.showSnackbar('Signup Successful!!');
        this.router.navigate(['home']);
      }
      if (response.error['error'].username) {
        this.errorMsg = 'Username alerady exists';
      }
      if (response.error.error.email && response.error.error.email[0] == 'This field must be unique.') {
        this.errorMsg = 'Email already exists';
        this.isLoading = false;
      } else {
        this.isLoading = false;
        console.log('response after signup =', response);
        this.api.showSnackbar('Signup Successful!!');
      }

    })

  }
  onImageUpload(event) {
    if (event.target.files.length > 0) {
      this.profilePic = event.target.files[0];
      this.profilePicUploaded = true;
      console.log("this.profilePic", this.profilePic);
    }
  }

}
