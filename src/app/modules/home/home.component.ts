import { Component, OnInit } from '@angular/core';
import { CommonApiService } from '../../api/services/common-api.service';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private api: CommonApiService,
    private http: HttpClient) { }

  userObj = {};
  files = [];

  ngOnInit() {
    this.userObj = this.api.getUserObj();
    console.log('userobj = ', this.userObj);
    this.showFiles('True');
  }

  showFiles(bool) {
    this.userObj = this.api.getUserObj();
    var token = this.api.getToken();
    if (this.userObj && bool) {
      this.api.getRequestWithToken('users/' + this.userObj['id'], token)
        .subscribe(response => {
          this.files = response.snaps;
        })
    }
  }

  deleteImage(imageObj) {
    var token = this.api.getToken();
    console.log(this.files, imageObj);
    this.api.deleteReqeust('snaps/' + imageObj.id, token)
      .subscribe(response => {
        const index = this.files.indexOf(imageObj);
        this.files.splice(index, 1);
        this.api.showSnackbar('File deleted successfully');
      })
  }
}
