import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonApiService } from '../../api/services/common-api.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
  imageFile: any;
  userObj: any;
  profilePicUploaded: boolean = false;
  uploadInProgress: boolean = false;
  constructor(private router: Router,
    private api: CommonApiService) { }

  ngOnInit() {
    if (!this.api.getUserObj()) {
      this.router.navigate(['login']);
    } else {
      this.userObj = this.api.getUserObj();
    }
  }

  uploadFile() {
    this.uploadInProgress = true;
    var dataObj = {
      "image": this.imageFile,
      "user": this.userObj.id
    }
    console.log("debba", this.imageFile);
    var data = new FormData();
    for (var key in dataObj) {
      data.append(key, dataObj[key]);
    }
    var token = this.api.getToken();
    console.log("data", data);
    this.api.postRequestWithCustomToken("snaps/", data, token).subscribe(response => {
      if (response.error) {
        this.api.showSnackbar('Something went wrong refresh and try again')
      }
      else {
        this.api.showSnackbar('Image uploaded successfully');
        this.router.navigate(['home']);
      }
      this.uploadInProgress = false;
    })
  }

  /**
  * Function to add multiple uploded files into an array
  * @param event contains the file that has been uploaded
  */
  onImageUpload(event) {
    if (event.target.files.length > 0) {
      this.imageFile = event.target.files[0];
      this.profilePicUploaded = true;
      console.log("this.imageFile", this.imageFile);
    }
  }


}
