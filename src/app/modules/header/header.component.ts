import { Component, OnInit } from '@angular/core';
import { CommonApiService } from '../../api/services/common-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private api: CommonApiService,
    private router: Router) { }
  userObj = {};
  ngOnInit() {
    this.api.currentMessage.subscribe(message => 
      {
        console.log("In current message");
        if(message == 'login'){
          console.log("In login message");
         this.userObj = this.api.getUserObj();
        }
        if(message == 'logout'){
          this.userObj = this.api.getUserObj();
        }
      });
    this.userObj = this.api.getUserObj();
  }

  logout() {
    console.log('logout');
    localStorage.removeItem('userObj');
    this.router.navigateByUrl('/login', { skipLocationChange: true }).then(() =>
      this.router.navigate(["home"]));
    this.userObj = null;
  }

  showUserImage(bool) {
    console.log('bolean');
    this.userObj = this.api.getUserObj();
  }



}
