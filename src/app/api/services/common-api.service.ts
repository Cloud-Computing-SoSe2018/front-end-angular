import {
  Injectable
} from '@angular/core';

import { Response, Headers } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()
export class CommonApiService {
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();

  private API_ENDPOINT = 'http://18.194.172.73/';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient, private cookie: CookieService, private snackbar: MatSnackBar, ) {

  }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }

  getToken() {
    if (this.cookie.get('token') && localStorage.getItem('userObj')) {
      var token = this.cookie.get('token');
      return token;
    } else {
      return null;
    }
  };

  //Function to make GET api call
  getRequest(url: any): Observable<any> {

    let token = this.getToken();
    return this.http.get(this.API_ENDPOINT + url, this.httpOptions)
      .map((response: Response) => {
        return response;
      })
      .catch((error: Error) => {
        return [Observable.throw(error)];
      });
  }

  //Function to make POST api call
  postRequest(url: any, data: any): Observable<any> {
    let token = this.getToken();
    if (token) {
      this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'JWT ' + token);
    }
    return this.http.post(this.API_ENDPOINT + url, data, this.httpOptions)
      .map((response: Response) => {
        console.log('response after api =', response);
        return response;
      })
      .catch((error: Error) => {
        return [Observable.throw(error)];
      });
  }

  //Function to make POST api call
  postRequestWithCustomToken(url: any, data: any, token: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': 'JWT ' + token })
    };

    return this.http.post(this.API_ENDPOINT + url, data, httpOptions)
      .map((response: Response) => {
        console.log('response after api =', response);
        return response;
      })
      .catch((error: Error) => {
        return [Observable.throw(error)];
      });
  }

  //Function to make get api call with token
  getRequestWithToken(url: any, token): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': 'JWT ' + token })
    };
    return this.http.get(this.API_ENDPOINT + url, httpOptions)
      .map((response: Response) => {
        return response;
      })
      .catch((error: Error) => {
        return [Observable.throw(error)];
      });
  }

  //Function to make DELETE api call
  deleteReqeust(url: any, token): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': 'JWT ' + token })
    };
    return this.http.delete(this.API_ENDPOINT + url, httpOptions)
      .map((response: Response) => {
        return response;
      })
      .catch((error: Error) => {
        return [Observable.throw(error)];
      });
  }

  postFormData(url: String, data): Observable<any> {
    var headers = new Headers();

    headers.append('Content-Type', 'application/json')
    return this.http.post(this.API_ENDPOINT + url, data)
      .map((response: Response) => {
        console.log('response after api =', response);
        return response;
      }).catch((error: Response) => {
        if (error.status === 404) {
          return Observable.throw(error);
        }
        return Observable.throw(error);
      });
  }

  postFormData2(url: String, data): Observable<any> {
    let token = this.getToken();
    console.log("token", token);
    var headers = new Headers();
    headers.append('Authorization', 'JWT ' + token);
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'JWT ' + token);
    return this.http.post(this.API_ENDPOINT + url, data, this.httpOptions)
      .map((response: Response) => {
        console.log('response after api =', response);
        return response;
      }).catch((error: Response) => {
        if (error.status === 404) {
          return Observable.throw(error);
        }
        return Observable.throw(error);
      });
  }

  postFormDataWithToken(url: String, data): Observable<any> {
    let token = this.getToken();
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'JWT ' + token);
    console.log("token", token, this.httpOptions.headers);
    return this.http.post(this.API_ENDPOINT + url, data)
      .map((response: Response) => {
        console.log('response after api =', response);
        return response;
      }).catch((error: Response) => {
        if (error.status === 404) {
          return Observable.throw(error);
        }
        return Observable.throw(error);
      });
  }

  login(data) {

    return this.postRequest('jwt-auth/', data);

  };

  signup(data, url) {
    return this.postFormData(url, data);
  };

  showSnackbar(msg) {
    this.snackbar.open(msg, '', {
      duration: 2500,
      verticalPosition: 'bottom',
      horizontalPosition: 'left',
    });
  };

  getUserObj() {
    if (localStorage.getItem('userObj')) {
      return JSON.parse(localStorage.getItem('userObj'));
    } else {
      return null;
    }
  }
}
