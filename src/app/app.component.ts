import { Component } from '@angular/core';
import { CommonApiService } from './api/services/common-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  userObj: any;
  constructor(private cookie: CookieService,
    private router: Router,
    private route: ActivatedRoute,
    private api: CommonApiService) {  }

  ngOnInit() {
    this.api.currentMessage.subscribe(message => 
    {
      console.log("In current message");
      if(message == 'login'){
        console.log("In login message");
       this.userObj = this.api.getUserObj();
      }
      if(message == 'logout'){
        this.userObj = this.api.getUserObj();
      }
    });
    this.userObj = JSON.parse(localStorage.getItem('userObj'));
  }
}
